import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  data:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private Database: DatabaseProvider,
              public alertCtrl: AlertController) {
    let params: any = this.navParams.get('params');
    this.data = params;
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  update(){
    console.log("HASIL EDIT ",this.data);
    this.Database.update(this.data).then(res=>{
      const alert = this.alertCtrl.create({
        title: 'Successfully',
        subTitle: 'Sukses Update Data',
        buttons: ['OK']
      });
      alert.present();
    }).catch(err=>{
      console.log(JSON.stringify(err));
      const alert = this.alertCtrl.create({
        title: 'Failed',
        subTitle: 'Gagal Update Data',
        buttons: ['OK']
      });
      alert.present();
    })
  }

}
