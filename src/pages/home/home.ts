import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { DetailPage } from '../detail/detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  label:any;
  data:any= [];
  isHidden=true;
  search='';
  listData=false;
  listSearch=true;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              private Database: DatabaseProvider
              ) {

      this.openDB();
  }

  openDB(){
    this.Database.openDB().then(res => {
      this.get_all();
    }).catch(err=>{
      console.log("error db "+ err);
    });
  }

  get_all(){
    this.listData=false;
    this.listSearch=true;
    this.Database.getAll().then(res=>{
      console.log(res);
      let dataAll = [];
      for(let i = 0; i < res['rows'].length; i++){
        dataAll.push(res['rows']['item'](i));
      }
      this.data = dataAll;
      console.log(this.data);
    }).catch(err=>{
      console.log(err);
    })
  }

  get_search(search){
    this.listData=true;
    this.listSearch=false;
    this.Database.getSearch(search).then(res=>{
      console.log(res);
      let dataAll = [];
      for(let i = 0; i < res['rows'].length; i++){
        dataAll.push(res['rows']['item'](i));
      }
      this.data = dataAll;
      console.log(this.data);
    }).catch(err=>{
      console.log(err);
    })
  }

  simpan(){
    this.Database.insert(this.label).then(res=>{
      this.label = "";
      const alert = this.alertCtrl.create({
        title: 'Successfully',
        subTitle: 'Sukses Insert Data',
        buttons: ['OK']
      });
      alert.present();
      this.get_all();
    }).catch(err=>{
      console.log(JSON.stringify(err));
      const alert = this.alertCtrl.create({
        title: 'Failed',
        subTitle: 'Gagal Insert Data',
        buttons: ['OK']
      });
      alert.present();
    })
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.get_all();
      refresher.complete();
    });
  }


  edit(data){
    // console.log(data);
    this.navCtrl.push(DetailPage,{params: data});
  }
  
  delete(data){
    console.log("HASIL DELETE ",data);
    this.Database.delete(data).then(res=>{
      const alert = this.alertCtrl.create({
        title: 'Successfully',
        subTitle: 'Sukses Delete Data',
        buttons: ['OK']
      });
      alert.present();
      this.get_all();
    }).catch(err=>{
      console.log(JSON.stringify(err));
      const alert = this.alertCtrl.create({
        title: 'Failed',
        subTitle: 'Gagal Delete Data',
        buttons: ['OK']
      });
      alert.present();
    })
  }

  showConfirmDelete(data) {
    const confirm = this.alertCtrl.create({
      title: 'Want Delete This data ?',
      // message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked'+ data);
            this.delete(data);
          }
        }
      ]
    });
    confirm.present();
  }

  filter(){
    if(this.isHidden == false){
      this.isHidden = true;
    }else{
      this.isHidden = false;
    }
  }

  getFilter(ev){
    this.search = ev.target.value;
    this.get_search(this.search);
    console.log(this.search);
  }

}
