import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  db:any;
  isOpen:Boolean=false;

  constructor(private sqllite: SQLite) {
    console.log('Hello DatabaseProvider Provider');
  }

  createDatabase(){
    return new Promise((resolve, reject) => {
      this.sqllite.create({
        name: 'todo.db',
        location: 'default'
      }).then((db:SQLiteObject)=>{
        this.isOpen = true;
        resolve(db);
      }).catch(err=>reject(err));
    })
  }

  openDB(){
    return new Promise((resolve, reject)=> {
      if(this.isOpen){
        console.log('has opened')
        resolve({})
      }else{
        this.createDatabase().then(db=>{
          this.db = db;
          this.db.executeSql("CREATE TABLE IF NOT EXISTS tb_todo (todo_id INTEGER PRIMARY KEY AUTOINCREMENT, todo_label TEXT)",[]).then(res=>resolve(res)).catch(err=>reject(err));
        }).catch(err=>reject(err))
      }     
    })
  }

  getAll(){
    return new Promise((resolve, reject)=> {
        this.db.executeSql("SELECT * FROM tb_todo",[])
        .then(res=>resolve(res))
        .catch(err=>reject(err));
    })
  }

  getSearch(data){
    return new Promise((resolve, reject)=> {
        this.db.executeSql("SELECT * FROM tb_todo WHERE todo_label LIKE '%"+ data +"%'",[])
        .then(res=>resolve(res))
        .catch(err=>reject(err));
    })
  }

  insert(label){
    return new Promise((resolve, reject)=> {
        this.db.executeSql("INSERT INTO tb_todo (todo_label) VALUES (?)",[label])
        .then(res=>resolve(res))
        .catch(err=>reject(err));
    })
  }

  update(data){
    return new Promise((resolve, reject)=> {
      this.db.executeSql("UPDATE tb_todo SET todo_label=? WHERE todo_id=?",[data.todo_label,data.todo_id])
      .then(res=>resolve(res))
      .catch(err=>reject(err));
    })
  }

  delete(data){
    return new Promise((resolve, reject)=> {
      this.db.executeSql("DELETE FROM tb_todo WHERE todo_id=?",[data.todo_id])
      .then(res=>resolve(res))
      .catch(err=>reject(err));
    })
  }
}
